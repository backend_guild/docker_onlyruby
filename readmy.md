## Описание
Докер-контейнер содержит nginx, puma, ruby, mysql, mongodb и phpmyadmin.
Порты и доступы описаны в docker-compose.yml

## Установка Docker
Для запуска контейнера необходимо установить docker и docker-compose. Установка под linux описана ниже
В windows для работы docker'а требуется Professional версия ОС + аппаратная поддержка виртуализации, 
что усложняет использование. Установка под windows здесь не описана. 

Установка docker под ubuntu:
```
$ sudo apt-get update
$ sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"
$ cat /etc/apt/sources.list.d/additional-repositories.list 
$ sudo apt-get update
$ sudo apt-get -y  install docker-ce docker-compose
$ sudo usermod -aG docker $USER

```

Для проверки запустите следующие команды:
```
$ sudo docker -v
$ sudo docker-compose -v

```
Если установка прошла удачно, команды должны вывести информацию о версиях данных пакетов.

## Запуск контейнеров
Сначала необходимо скомпилировать образ:
``` 
$ sudo docker-compose build
```

После разворачивания образа можно проверить работоспособность контейнеров:
```
$ sudo docker-compose run app ruby -v
$ sudo docker-compose run app bundler -v
```

Запуск контейнера:
```
$ sudo docker-compose up
```

Запуск helloworld из контейнера:
```
$ sudo docker-compose exec app ruby helloworld.rb
```

Остановка контейнера:
``` 
$ sudo docker-compose down
```